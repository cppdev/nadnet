#include <NadNet/NadNet.hpp>
#include <iostream>
#include <sstream>

int main ()
{
	NadNet::NadNet app;
	app.set_static ("/static")
	   //EXTENDED_VERSION: .route ("/", NadNet::respond_manager ().set ([]() {
	   //EXTENDED_VERSION:  	std::cout << "hello world\r\n" << std::endl;		
	   //EXTENDED_VERSION:  	return NadNet::response (NadNet::status_line ("200", "1.1"), {}, "<h1>I love you! I'm working</h1>");
	   //EXTENDED_VERSION: }))
	   //EXTENDED_VERSION: .route ("/users/{user:[a-zA-Z0-9]*}", NadNet::respond_manager ().set ([](NadNet::path_vars v) {
	   //EXTENDED_VERSION:  	for (auto& x : v)
	   //EXTENDED_VERSION:  		std::cout << x.first << " " << x.second << std::endl; 
	   //EXTENDED_VERSION:  	return NadNet::response ();
	   //EXTENDED_VERSION: }))
	   //EXTENDED_VERSION: .route ("/contest/{id#int:[0-9]*}", NadNet::respond_manager ().set ([](NadNet::path_vars v) {
	   //EXTENDED_VERSION:  	for (auto& x : v.vars)
	   //EXTENDED_VERSION:  		std::cout << x.first << " " << x.second.value << " " << x.second.type << std::endl; 
	   //EXTENDED_VERSION: 	int id = v["id"];
	   //EXTENDED_VERSION: 	std::string s (v["id"]);
	   //EXTENDED_VERSION: 	std::cout << "int = " << id << " string = " << s << std::endl;
	   //EXTENDED_VERSION:  	return NadNet::response ();
	   //EXTENDED_VERSION: }))
	   //EXTENDED_VERSION: .route ("/users/{user:[a-zA-Z0-9]*}/{tab:.*}", NadNet::respond_manager ().set ([](NadNet::path_vars v) {
	   //EXTENDED_VERSION:  	for (auto& x : v)
	   //EXTENDED_VERSION:  		std::cout << x.first << " " << x.second << std::endl; 
	   //EXTENDED_VERSION:  	return NadNet::response ();
	   //EXTENDED_VERSION: }))
	   .route ("/", []() {
	     	return NadNet::response (NadNet::status_line ("200"), {{"Content-Type", "text/html; charset=utf-8"}}, "<h1>I love you! I'm working</h1>");
	   })
	   .route ("/users/{user:[a-zA-Z0-9]*}", [](NadNet::path_vars v) {
			std::stringstream ss;
			for (auto& x : v.vars)
				ss << x.first << " " << x.second.value << " " << x.second.type << "<br>"; 
	     	return NadNet::response (NadNet::status_line ("200"), {{"Content-Type", "text/html; charset=utf-8"}}, "<h1>" + ss.str () + "</h1>");
	   })
	   .route ("/contest/{id#int:[0-9]*}", [](NadNet::path_vars v) {
	    	for (auto& x : v.vars) {
				std::cout << x.first << " " << x.second.value << " " << x.second.type << std::endl; 
			}
			int id = v["id"];
			std::string s (v["id"]);
			std::cout << "int = " << id << " string = " << s << std::endl;
	    	return NadNet::response ();
	   })
	   //SIMPLIFIED_VERSION: .route ("/contest/{id#int}", [](NadNet::path_vars v) {
	   //SIMPLIFIED_VERSION:  	for (auto& x : v.vars)
	   //SIMPLIFIED_VERSION:  		std::cout << x.first << " " << x.second.value << " " << x.second.type << std::endl; 
	   //SIMPLIFIED_VERSION:  	return NadNet::response ();
	   //SIMPLIFIED_VERSION: })
	   .route ("/users/{user:[a-zA-Z0-9]*}/{tab:.*}", [](NadNet::path_vars v) {
			for (auto& x : v.vars)
				std::cout << x.first << " " << x.second.value << " " << x.second.type << std::endl; 
			return NadNet::response ();
	   })
	;

	const unsigned short port = 8888; const unsigned char cores = ((std::thread::hardware_concurrency () - 1 > 0) ? (std::thread::hardware_concurrency () - 1) : 1);
	std::cout << "Listening on :" << port << " with " << (int)cores << " cores" << std::endl;
	app.run (port, cores);
}
